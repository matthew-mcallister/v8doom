find_path(V8_INCLUDE_DIR
    NAMES v8.h
    HINTS /usr/include/node /usr/include/v8
        /usr/local/include/node /usr/local/include/v8)
mark_as_advanced(V8_INCLUDE_DIR)

find_library(V8_LIBRARY NAMES v8)
find_library(V8_BASE_LIBRARY NAMES v8_libbase)
find_library(V8_PLATFORM_LIBRARY NAMES v8_libplatform)
find_library(V8_SAMPLER_LIBRARY NAMES v8_libsampler)
mark_as_advanced(V8_LIBRARY)
mark_as_advanced(V8_BASE_LIBRARY)
mark_as_advanced(V8_PLATFORM_LIBRARY)
mark_as_advanced(V8_SAMPLER_LIBRARY)

include("${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake")
find_package_handle_standard_args(V8
    DEFAULT_MSG
    V8_INCLUDE_DIR
    V8_LIBRARY 
    V8_BASE_LIBRARY
    V8_PLATFORM_LIBRARY
    V8_SAMPLER_LIBRARY)

set(V8_LIBRARIES
    "${V8_LIBRARY}"
    "${V8_BASE_LIBRARY}"
    "${V8_PLATFORM_LIBRARY}"
    "${V8_SAMPLER_LIBRARY}")
set(V8_INCLUDE_DIRS "${V8_INCLUDE_DIR}")

add_library(v8 INTERFACE)
target_link_libraries(v8 INTERFACE ${V8_LIBRARIES})
target_include_directories(v8 SYSTEM INTERFACE ${V8_INCLUDE_DIRS})
