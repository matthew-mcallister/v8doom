#include <v8.h>
#include <libplatform/libplatform.h>

#include "m_argv.h"
#include "script/interop.h"

namespace script {

// V8 really should just provide an `IsInitialized` function.
bool g_have_platform = false;

Platform::Platform() {
    if (g_have_platform) {
        throw "tried to initialize V8 platform twice";
    }

    g_have_platform = true;

    v8::V8::InitializeICUDefaultLocation(myargv[0]);
    v8::V8::InitializeExternalStartupData(myargv[0]);
    m_platform = v8::platform::NewDefaultPlatform();
    v8::V8::InitializePlatform(m_platform.get());
    v8::V8::Initialize();
}

Platform::~Platform() {
    v8::V8::Dispose();
    v8::V8::ShutdownPlatform();
}

auto Platform::create_isolate() -> Isolate {
    v8::Isolate::CreateParams create_params;
    create_params.array_buffer_allocator =
        v8::ArrayBuffer::Allocator::NewDefaultAllocator();
    auto isolate = v8::Isolate::New(create_params);
    isolate->Enter();
    return Isolate{isolate};
}

auto Isolate::instantiate_module(const char* name, const char* source) ->
    Object
{
    auto scope = handle_scope();

    v8::Local<v8::String> name_s{v8::String::NewFromUtf8(inner(), name)};
    v8::ScriptOrigin origin{
        name_s,
        v8::Local<v8::Integer>(),
        v8::Local<v8::Integer>(),
        v8::Local<v8::Boolean>(),
        v8::Local<v8::Integer>(),
        v8::Local<v8::Value>(),
        v8::Local<v8::Boolean>(),
        v8::Local<v8::Boolean>(),
        v8::Boolean::New(inner(), true), // is_module
        v8::Local<v8::PrimitiveArray>(),
    };
    v8::ScriptCompiler::Source src{
        v8::String::NewFromUtf8(inner(), source),
        origin,
    };

    auto context = current_context();

    auto try_catch = this->try_catch();
    auto mod = try_catch.get_checked(
        v8::ScriptCompiler().CompileModule(inner(), &src));
    assert(try_catch.get_checked(
        mod->InstantiateModule(context, nullptr)));
    assert(mod->GetStatus() == v8::Module::Status::kInstantiated);

    try_catch.get_checked(mod->Evaluate(context));
    assert(mod->GetStatus() == v8::Module::Status::kEvaluated);

    return Value(*this, mod->GetModuleNamespace()).get_object();
}

}
