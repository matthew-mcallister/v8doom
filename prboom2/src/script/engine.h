#ifndef __SCRIPT__
#define __SCRIPT__

#include <exception>
#include <optional>
#include <vector>

#include <v8.h>

#include "script/interop.h"

namespace script {

class Engine {
public:
    Engine();
    ~Engine();

    Engine(const Engine&) = delete;
    Engine(Engine&&) = delete;
    Engine& operator=(const Engine&) = delete;
    Engine& operator=(Engine&&) = delete;

    static auto create() -> Engine;

    auto isolate() -> Isolate {
        return m_isolate;
    }

    auto context() -> v8::Global<v8::Context>& {
        return m_context;
    }

    void add_component(std::string name, std::string content);
    void update();

private:
    // TODO: Actually want component management done in JS.
    class Component {
        Component(Isolate isolate, std::string name, std::string source);

        auto isolate() -> Isolate {
            return m_isolate;
        }

        auto object() -> Object {
            return isolate().get_global(m_namespace);
        }

        void update();

        std::string m_name;
        Isolate m_isolate;
        v8::Global<v8::Object> m_namespace;

        friend class Engine;
    };

    Platform m_platform;
    Isolate m_isolate;
    v8::Global<v8::Context> m_context;
    std::vector<Component> m_components;
};


void LoadScripts(Engine& engine);

}

#endif
