#include <cassert>
#include <iostream>
#include <string>
#include <utility>

#include <v8.h>

#include "script/engine.h"

#include "info.h"
#include "lprintf.h"
#include "i_main.h"
#include "m_argv.h"
#include "script/interop.h"
#include "w_wad.h"

namespace script {

namespace detail {

using Callback = void (&)(script::Args);

template<Callback func>
static void callback(const v8::FunctionCallbackInfo<v8::Value>& info) noexcept
{
    script::Args args{info};
    try {
        func(args);
    } catch (const script::ValueException& e) {
        // Let exception propagate to caller
    } catch (const std::exception& e) {
        auto isolate = args.isolate();
        auto context = isolate.current_context();

        auto key = isolate.string_literal<"Error">();
        auto error_cons = v8::Local<v8::Function>::Cast(
            context->Global()->Get(context, key).ToLocalChecked());

        auto msg = v8::Local<v8::Value>::Cast(isolate.string(e.what()));
        auto ex = error_cons->NewInstance(context, 1, &msg).ToLocalChecked();

        isolate.throw_exception(ex);
    }
}

static void print(script::Args args) {
    for (int i = 0; i < args.len(); i++) {
        auto s = args[i].utf8_value();
        assert(s.length() > 0);
        if (i > 0) {
            lprintf(LO_INFO, " ");
        }
        lprintf(LO_INFO, "%s", s.c_str());
    }
    lprintf(LO_INFO, "\n");
}

static void set_deh_state(script::Args args) {
    auto isolate = args.isolate();
    auto index = args[0].get_int();
    auto state = args[1].get_object();
    auto sprite = state.get<"sprite">().get_string();
    auto frame = state.get<"frame">().get_int();
    auto tics = state.get<"tics">().get_int();

    auto action = state.has<"action">()
        ? state.get<"action">().get_string()
        : std::string();
    auto nextstate = state.has<"nextstate">()
        ? state.get<"nextstate">().get_int()
        : -1;

    lprintf(LO_INFO, "assign_state(%d, '%s', %d, %d, '%s', %d)\n",
        index, sprite.c_str(), frame, tics, action.c_str(), nextstate);

    //if (nextstate < 0) {
    //    nextstate = index + 1;
    //}

    //if (index < 0 || index >= NUMSTATES || nextstate >= NUMSTATES) {
    //    throw script::NativeError{"state number out of bounds"};
    //}

    //assign_state(index, sprite.c_str(), frame, tics, action.c_str(),
    //    nextstate);
}

}

auto Engine::create() -> Engine {
    return Engine{};
}

Engine::Engine() : m_isolate(m_platform.create_isolate()) {
    auto scope = m_isolate.handle_scope();
    auto isolate = m_isolate.inner();

    auto global = v8::ObjectTemplate::New(isolate);
    auto print = v8::FunctionTemplate::New(
        isolate, detail::callback<detail::print>);
    global->Set(isolate, "print", print);
    auto set_deh_state = v8::FunctionTemplate::New(
        isolate, detail::callback<detail::set_deh_state>);
    global->Set(isolate, "set_deh_state", set_deh_state);

    auto context = v8::Context::New(isolate, nullptr, global);
    if (context.IsEmpty()) {
        throw "could not initialize context";
    }
    context->Enter();

    m_context.Reset(isolate, context);
}

Engine::~Engine() {
    isolate().exit();
    isolate().dispose();
}

void Engine::add_component(std::string name, std::string source) {
    m_components.push_back(Component{isolate(), name, source});
}

void Engine::update() {
    try {
        for (auto& component : m_components) {
            component.update();
        }
    } catch (script::ValueException& ex) {
        std::cout << ex.value().utf8_value() << std::endl;
    }
}

Engine::Component::Component(
    Isolate isolate,
    std::string name,
    std::string source
) : m_name(std::move(name)) , m_isolate(isolate) {
    m_namespace.Reset(
        isolate.inner(),
        isolate.instantiate_module(m_name.c_str(), source.c_str())
            .to_global()
    );
}

void Engine::Component::update() {
    auto scope = isolate().handle_scope();
    auto try_catch = isolate().try_catch();
    object().get<"update">().get_function().call();
    try_catch.propagate();
}

void LoadScripts(Engine& engine) {
    using namespace std::string_literals;
    auto name = "SCRIPT"s;
    auto lump = W_GetNumForName(name.c_str());
    auto ptr = W_CacheLumpNum(lump);
    auto size = W_LumpLength(lump);
    std::string data;
    data.assign(static_cast<const char*>(ptr), size);
    try {
        engine.add_component(name, data);
    } catch (script::ValueException& ex) {
        std::cout << ex.value().utf8_value() << std::endl;
    }
}

}
