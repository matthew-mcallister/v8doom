#ifndef __SCRIPT_INTEROP__
#define __SCRIPT_INTEROP__

#include <array>
#include <cassert>
#include <cstdint>
#include <initializer_list>
#include <optional>
#include <sstream>
#include <utility>

#include <v8.h>

#include "utility.h"

namespace script {

class Isolate;
class Function;
class Object;
class TryCatch;

/// Initializes V8.
class Platform {
public:
    Platform();
    ~Platform();

    auto create_isolate() -> Isolate;

private:
    std::unique_ptr<v8::Platform> m_platform;
};

// TODO: This should own the isolate.
class Isolate {
public:
    Isolate() = default;
    Isolate(v8::Isolate* isolate) : m_isolate(isolate) {
        assert(m_isolate);
    }

    Isolate(const Isolate&) = default;
    Isolate(Isolate&&) = default;
    Isolate& operator=(const Isolate&) = default;
    Isolate& operator=(Isolate&&) = default;

    // Isolate is just a wrapper around v8::Isolate* and doesn't call
    // Dispose().
    ~Isolate() = default;

    operator v8::Isolate*() {
        return m_isolate;
    }

    auto current_context() -> v8::Local<v8::Context> {
        auto context = m_isolate->GetCurrentContext();
        assert(!context.IsEmpty());
        return context;
    }

    // TODO: Maybe take a closure and automatically handle rethrowing?
    auto try_catch() -> TryCatch;

    auto handle_scope() -> v8::HandleScope {
        return v8::HandleScope{m_isolate};
    }

    auto string(const char* str) -> v8::Local<v8::String> {
        return v8::String::NewFromUtf8(
            m_isolate, str, v8::NewStringType::kNormal)
            .ToLocalChecked();
    }

    template<FixedString S>
    auto string_literal() -> v8::Local<v8::String> {
        return v8::String::NewFromUtf8(
            m_isolate, &*S.data, v8::NewStringType::kInternalized)
            .ToLocalChecked();
    }

    auto get_global(v8::Global<v8::Object>& obj) const -> Object;

    void throw_exception(v8::Local<v8::Value> exception) {
        m_isolate->ThrowException(exception);
    }

    auto inner() const -> v8::Isolate* {
        return m_isolate;
    }

    /// Instantiates a JS module and returns the resulting namespace
    /// object.
    auto instantiate_module(const char* name, const char* source) -> Object;

    inline void dispose() { m_isolate->Dispose(); }
    inline void enter() { m_isolate->Enter(); }
    inline void exit() { m_isolate->Exit(); }

private:
    v8::Isolate* m_isolate;
};

/// Wraps a `Local<Value>` with the corresponding isolate.
class Value {
public:
    Value(Isolate isolate, v8::Local<v8::Value>&& value)
        : m_isolate(isolate), m_value(value)
    {
        assert(!m_value.IsEmpty());
    }

    Value(Isolate isolate, const v8::Local<v8::Value>& value)
        : Value(isolate, v8::Local{value})
    {}

    static auto from_string(Isolate isolate, const char* s) -> Value {
        return Value(isolate, v8::Local{isolate.string(s)});
    }

    auto get_int() const -> int32_t;

    auto get_string() const -> std::string;

    auto get_object() const -> Object;

    auto get_function() const -> Function;

    auto utf8_value() const -> std::string;

    auto context() const -> v8::Local<v8::Context> {
        return isolate().current_context();
    }

    auto isolate() const -> Isolate {
        return m_isolate;
    }

    auto value() const -> v8::Local<v8::Value> {
        return m_value;
    }

private:
    Isolate m_isolate;
    v8::Local<v8::Value> m_value;
};

/// Wraps a JavaScript exception so it can easily be thrown from or
/// caught by native code. The exception is only valid for the lifetime
/// of the current handle scope.
struct ValueException : public std::exception {
    ValueException(Isolate isolate, v8::Local<v8::Value> exception) :
        m_value(isolate, exception) {}
    ValueException(Value exception) : m_value(exception) {}
    ValueException(Isolate isolate, const char* s) :
        m_value(Value::from_string(isolate, s)) {}

    // TODO: Maybe convert the value to a string and cache it?
    virtual const char* what() const noexcept {
        return "scripting engine exception";
    }

    auto value() const -> Value {
        return m_value;
    }

private:
    Value m_value;
};

class Object {
public:
    Object(v8::Local<v8::Object>&& object) : m_object(object) {
        assert(!m_object.IsEmpty());
    }

    Object(v8::Local<v8::Object>& object) : Object(v8::Local{object}) {}

    Object(Isolate isolate, v8::Global<v8::Object>& object) :
        Object(isolate.get_global(object))
    {}

    template<FixedString K>
    auto get() const -> Value;

    template<FixedString K>
    auto has() const -> bool;

    auto isolate() const -> Isolate {
        return Isolate{m_object->GetIsolate()};
    }

    auto context() const -> v8::Local<v8::Context> {
        return isolate().current_context();
    }

    auto inner() const -> v8::Local<v8::Object> {
        return m_object;
    }

    auto to_global() const -> v8::Global<v8::Object> {
        return {isolate().inner(), inner()};
    }

private:
    v8::Local<v8::Object> m_object;
};

/// A basic wrapper around v8::TryCatch that simplifies working directly
/// with V8 APIs.
class TryCatch {
public:
    TryCatch(Isolate isolate) : m_isolate(isolate),
        m_try_catch(isolate.inner()) {}

    auto isolate() const -> Isolate {
        return m_isolate;
    }

    bool caught() const {
        return m_try_catch.HasCaught();
    }

    void propagate() {
        if (caught()) {
            throw ValueException{isolate(), m_try_catch.Exception()};
        }
    }

    /// Asserts that a JS exception was thrown and propagates it.
    void expect_propagate() {
        assert(caught());
        propagate();
    }

    template<typename T>
    auto get_checked(v8::MaybeLocal<T> result) -> v8::Local<T> {
        v8::Local<T> val;
        if (!result.ToLocal(&val)) {
            expect_propagate();
        }
        return val;
    }

    template<typename T>
    auto get_checked(v8::Maybe<T> result) -> T {
        T val;
        if (!result.To(&val)) {
            expect_propagate();
        }
        return val;
    }

private:
    Isolate m_isolate;
    v8::TryCatch m_try_catch;
};

class Function {
public:
    Function(v8::Local<v8::Function>&& function) : m_function(function) {
        assert(!m_function.IsEmpty());
    }

    auto isolate() const -> Isolate {
        return Isolate{m_function->GetIsolate()};
    }

    auto inner() const -> v8::Local<v8::Function> {
        return m_function;
    }

    auto call() -> std::optional<Value> {
        std::array<v8::Local<v8::Value>, 0> ra;
        return call_inner(ra);
    }

private:
    template<std::size_t N>
    auto call_inner(std::array<v8::Local<v8::Value>, N>& args) ->
        std::optional<Value>;

    v8::Local<v8::Function> m_function;
};

class Args {
public:
    Args(const v8::FunctionCallbackInfo<v8::Value>& info)
        : m_info(info)
    {}

    auto operator[](int index) const -> Value {
        // No bounds check as out-of-bounds indexing returns `undefined`
        return Value{isolate(), m_info[index]};
    }

    auto len() const -> int {
        return m_info.Length();
    }

    auto isolate() const -> Isolate {
        return Isolate{m_info.GetIsolate()};
    }

    auto context() const -> v8::Local<v8::Context> {
        return isolate().current_context();
    }

private:
    const v8::FunctionCallbackInfo<v8::Value>& m_info;
};

inline auto Isolate::try_catch() -> TryCatch {
    return TryCatch{m_isolate};
}

inline auto Isolate::get_global(v8::Global<v8::Object>& obj) const -> Object {
    return Object{obj.Get(m_isolate)};
}

inline auto Value::get_int() const -> int32_t {
    if (!m_value->IsInt32()) {
        throw ValueException{isolate(), "expected int"};
    }
    return v8::Local<v8::Int32>::Cast(m_value)->Value();
}

inline auto Value::get_string() const -> std::string {
    if (!m_value->IsString()) {
        throw ValueException{isolate(), "expected string"};
    }
    return utf8_value();
}

inline auto Value::get_object() const -> Object {
    if (!m_value->IsObject()) {
        throw ValueException{isolate(), "expected object"};
    }
    return Object{v8::Local<v8::Object>::Cast(m_value)};
}

inline auto Value::get_function() const -> Function {
    if (!m_value->IsFunction()) {
        throw ValueException{isolate(), "expected function"};
    }
    return Function{v8::Local<v8::Function>::Cast(m_value)};
}

inline auto Value::utf8_value() const -> std::string {
    auto try_catch = isolate().try_catch();
    auto val = v8::String::Utf8Value{isolate().inner(), m_value};
    try_catch.propagate();
    assert(*val);
    return std::string{*val};
}

template<FixedString K>
inline auto Object::get() const -> Value {
    auto key = isolate().string_literal<K>();

    auto try_catch = isolate().try_catch();
    v8::Local<v8::Value> value;
    if (m_object->Get(context(), key).ToLocal(&value)) {
        return Value{isolate(), value};
    }
    try_catch.propagate();

    std::stringstream ss;
    ss << "expected field '" << K.data << "'" << std::endl;
    throw ValueException{isolate(), ss.str().c_str()};
}

template<FixedString K>
inline auto Object::has() const -> bool {
    auto key = isolate().string_literal<K>();

    auto try_catch = isolate().try_catch();
    bool has;
    if (m_object->Has(context(), key).To(&has)) {
        return has;
    }
    try_catch.propagate();

    unreachable();
}

template<std::size_t N>
inline auto Function::call_inner(std::array<v8::Local<v8::Value>, N>& args) ->
    std::optional<Value>
{
    v8::Local<v8::Value>* ptr = nullptr;
    if (!args.empty()) { ptr = &args.front(); }

    v8::Local<v8::Context> ctx = isolate().current_context();

    v8::Local<v8::Value> result;
    if (m_function->Call(
        ctx,
        // TODO: Maybe should set `this` to a dummy object rather than
        // global `this`?
        ctx->Global(),
        args.size(),
        ptr
    ).ToLocal(&result)) {
        return Value{isolate(), result};
    } else {
        return std::nullopt;
    }
}

}

#endif
