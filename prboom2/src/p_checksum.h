#ifdef __cplusplus
extern "C" {
#endif

extern void (*P_Checksum)(int);
extern void P_ChecksumFinal(void);
void P_RecordChecksum(const char *file);
//void P_VerifyChecksum(const char *file);

#ifdef __cplusplus
}
#endif
