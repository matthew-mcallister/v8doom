#ifndef __UTILITY__
#define __UTILITY__

#include <algorithm>
#include <compare>

#define unreachable() assert(false)

template<int N>
class FixedString {
public:
    constexpr inline FixedString(const char (&c)[N]) {
        std::copy_n(c, N, data);
    }

    constexpr auto operator<=>(const FixedString&) const
        -> std::strong_ordering = default;

    char data[N];
};

#endif
