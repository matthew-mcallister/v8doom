function init() {
    set_deh_state(174, {
        sprite: 'SPOS',
        frame: 0,
        tics: 10,
        action: 'A_Look',
    })
}

init()

let counter = 0

export function update() {
    counter += 1
    print('frame:', counter)
}
